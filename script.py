from prometheus_client import start_http_server, Gauge, Counter
import time
from glob import glob
import pandas as pd


PATH = "/data/Results"

triggers = Counter('scint_triggers', 'Count of scintillator triggers')

if __name__ == '__main__':
    # Start up the server to expose the metrics.
    start_http_server(8000)
    previous_timestamp = 0
    while True:
        filenames = sorted(glob(f"{PATH}/TriggerMonitor*.dat"))
        filename = filenames[-1]
        df = pd.read_csv(filename, delimiter="\t", header=None)
        df = df.set_index(1)
        df = df.loc[df[2] < 400000000]
        print(previous_timestamp)
        print(df.tail(10))
        if df.iloc[-1].name > previous_timestamp:
            try:
                triggers.inc(df.loc[previous_timestamp:].sum()[2]- df.loc[previous_timestamp][2])
            except KeyError:
                triggers.inc(df.sum()[2])
            previous_timestamp = df.iloc[-1].name
        time.sleep(1)
